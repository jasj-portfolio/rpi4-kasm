#!/usr/bin/env sh

# Minimum system requirements
echo "Minimum System Requirements"
echo "CPU: 2 cores"
echo "Memory: 4GB"
echo "Storage: 50GB"

# Ask for current version so script no longer has to be changed after every kasm update
echo "Please input the current version of KASM. example 1.14.0.3a7abb"
echo "1.14.0.3a7abb being the current version as of this scripts update."
read version

# Asking if there is a swap partition mainly in case your installing on server and a swap file already exists
echo "Is there already a swap partition?"
read swap
echo "$swap" | awk '{print tolower($0)}'

# First we install Docker
sudo apt update && sudo apt upgrade -y
sudo apt install curl ca-certificates gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo systemctl enable docker.service
sudo systemctl enable containerd.service

# Create a swap partition
if [ "$swap" == "no" ]
then
    sudo dd if=/dev/zero bs=1M count=1024 of=/mnt/1GiB.swap
    sudo chmod 600 /mnt/1GiB.swap
    sudo mkswap /mnt/1GiB.swap
    sudo swapon /mnt/1GiB.swap
    echo '/mnt/1GiB.swap swap swap defaults 0 0' | sudo tee -a /etc/fstab
else
    true
fi

# Now we install Kasm
cd /tmp
curl -o https://kasm-static-content.s3.amazonaws.com/kasm_release_$version.tar.gz
tar -xf kasm_release*.tar.gz
sudo bash kasm_release/install.sh

echo "Are you ready to reboot?"
read refresh

if [ "$refresh" == "yes" ]
then
    sudo reboot
elif [ "$refresh" == "no" ]
then
    echo "Please Don't forget to reboot."
    exit(0)
else
    echo "Something went wrong"
    exit(0)
fi

# Make sure to get your pi's IP address and your kasm admin login credentials then reboot
