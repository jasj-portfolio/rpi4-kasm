## Name
Rpi4 LAN Kasm

## Description
I'm setting up my Raspberry Pi 4 to run Docker and Kasm on my local network. This repo will contain an install script and any special config files I create. This is the minimum requirements to run KASM so it does tend to be a little slow sometimes.

Specs of Pi4 Server I tested the script on
- 4gb RAM
- 64gb Storage
- 1gb Swap
- Ubuntu server 20.04.5 LTS
- Ethernet connection

## Installation
- git clone https://gitlab.com/jasj-portfolio/rpi4-kasm.git
- cd rpi4-kasm
- chmod +x install.sh
- ./install.sh

## Usage
I am going to school for infosec so my config files and containers will reflect that. My biggest use case for this build will be studying malware in remnux

## Contributing
This should be pretty straight forward I'm not doing anything to crazy here, but if you do want to contribute please send me any ideas.

## License
GPL v3

